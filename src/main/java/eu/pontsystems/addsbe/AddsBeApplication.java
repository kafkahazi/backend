package eu.pontsystems.addsbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AddsBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(AddsBeApplication.class, args);
    }

}
