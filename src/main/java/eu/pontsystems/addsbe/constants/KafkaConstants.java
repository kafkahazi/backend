package eu.pontsystems.addsbe.constants;

public class KafkaConstants {
    public static final String KAFKA_TOPIC = "adds";
    public static final String GROUP_ID = "group_id";
    public static final String KAFKA_BROKER = "localhost:9092";
}
