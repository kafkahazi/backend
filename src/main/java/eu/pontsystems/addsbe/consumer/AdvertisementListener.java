package eu.pontsystems.addsbe.consumer;

import eu.pontsystems.addsbe.constants.KafkaConstants;
import eu.pontsystems.addsbe.model.Advertisement;
import eu.pontsystems.addsbe.service.AdvertisementService;
import lombok.AllArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AdvertisementListener {

    private final SimpMessagingTemplate template;
    private final AdvertisementService advertisementService;

    @KafkaListener(
            topics = KafkaConstants.KAFKA_TOPIC,
            groupId = KafkaConstants.GROUP_ID
    )
    public void listen(Advertisement advertisement) {
        System.out.println("sending via advertisement listener..");
        template.convertAndSend("/topic/group", advertisement);
        advertisementService.save(advertisement);
    }
}
