package eu.pontsystems.addsbe.controller;

import eu.pontsystems.addsbe.model.Advertisement;
import eu.pontsystems.addsbe.service.AdvertisementService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class Controller {

    //private final KafkaTemplate<String, Advertisement> kafkaTemplate;
    private final AdvertisementService advertisementService;

    @GetMapping(value = "/api/kafka/advertisements")
    public ResponseEntity<List<Advertisement>> getAllAdvertisements() {
        List<Advertisement> advertisements = advertisementService.findAll();
        return new ResponseEntity<>(advertisements, HttpStatus.OK);
    }

    @MessageMapping("/sendMessage")
    @SendTo("/topic/group")
    public Advertisement broadcastGroupMessage(@Payload Advertisement advertisement) {
        //Sending this message to all the subscribers
        return advertisement;
    }
}
