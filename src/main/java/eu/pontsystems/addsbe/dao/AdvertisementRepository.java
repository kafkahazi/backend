package eu.pontsystems.addsbe.dao;

import eu.pontsystems.addsbe.model.Advertisement;
import org.springframework.data.cassandra.repository.CassandraRepository;

import java.util.UUID;

public interface AdvertisementRepository extends CassandraRepository<Advertisement, UUID> {
}
