package eu.pontsystems.addsbe.model;

import lombok.Data;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Data
@Table
public class Advertisement {
    @PrimaryKey
    private UUID id = UUID.randomUUID();
    String name;
    String message;

}
