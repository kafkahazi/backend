package eu.pontsystems.addsbe.service;

import eu.pontsystems.addsbe.model.Advertisement;

import java.util.List;

public interface AdvertisementService {
    void save(Advertisement advertisement);
    List<Advertisement> findAll();
}
