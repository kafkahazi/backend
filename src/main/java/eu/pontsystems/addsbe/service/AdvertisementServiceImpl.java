package eu.pontsystems.addsbe.service;

import eu.pontsystems.addsbe.dao.AdvertisementRepository;
import eu.pontsystems.addsbe.model.Advertisement;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AdvertisementServiceImpl implements AdvertisementService {

    private final AdvertisementRepository advertisementRepository;

    @Override
    public void save(Advertisement advertisement) {
        advertisementRepository.save(advertisement);
    }

    @Override
    public List<Advertisement> findAll() {
        return advertisementRepository.findAll();
    }
}
